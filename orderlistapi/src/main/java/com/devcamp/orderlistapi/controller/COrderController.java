package com.devcamp.orderlistapi.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.orderlistapi.model.COrders;
import com.devcamp.orderlistapi.repository.IOrderRepository;



@CrossOrigin
@RestController
@RequestMapping("/")
public class COrderController {
    @Autowired
    IOrderRepository pOrderRepository;
    @GetMapping("/orders")
    public ResponseEntity<List<COrders>> getAllCustomers(){
        try{
            List<COrders> listOrders = new ArrayList<COrders>();

            pOrderRepository.findAll()
            .forEach(listOrders::add);
            return new ResponseEntity<>(listOrders, HttpStatus.OK);

        } catch (Exception ex) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
        
    }
}
