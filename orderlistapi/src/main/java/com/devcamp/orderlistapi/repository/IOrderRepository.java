package com.devcamp.orderlistapi.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.orderlistapi.model.COrders;

public interface IOrderRepository extends JpaRepository<COrders, Long> {
    
}
