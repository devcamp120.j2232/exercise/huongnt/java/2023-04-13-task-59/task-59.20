package com.devcamp.orderlistapi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class OrderlistapiApplication {

	public static void main(String[] args) {
		SpringApplication.run(OrderlistapiApplication.class, args);
	}

}
